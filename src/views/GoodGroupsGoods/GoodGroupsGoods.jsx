import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Table,
  Row,
  Col,
  Button,
  ButtonGroup,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter
} from "reactstrap";

import { PanelHeader } from "components";

const goodGroupsTableHeader = [
    {
      id: "name",
      name: "Name"
    },
    {
      id: "actions",
      name: "Actions"
    }
  ];
const goodsTableHeader = [
    {
      id: "name",
      name: "Name"
    },
    {
      id: "price",
      name: "Price"
    },
    {
      id: "quantity",
      name: "Quantity"
    },
    {
      id: "description",
      name: "Description"
    },
    {
      id: "actions",
      name: "Actions"
    }
  ];


class RegularTables extends React.Component {
  constructor(props) {
    super(props);
    this.handleOnclick = this.handleOnclick.bind(this);
    this.toggle = this.toggle.bind(this);
    this.state = {
      modalGroups: false,
      modalGoods: false,
      goodGroups: [
        {
          id: 0,
          name: "ungrouped",
          active: true
        },
        {
          id: 1,
          name: "Sirniyyat",
          active: false
        },
        {
          id: 2,
          name: "Ickiler",
          active: false
        }
      ],
      goods: [
        {
          id: 1,
          groupID: 0,
          name: "atash",
          description: "test description. It is a little bit longer then others. It's just for testing...",
          price: 0.5,
          quantity: 3
        },
        {
          id: 2,
          groupID: 0,
          name: "qagas",
          description: "test description. It is a little bit longer then others. It's just for testing...",
          price: 0.6,
          quantity: 6
        },
        {
          id: 3,
          groupID: 0,
          name: "dadasss",
          description: "test description. It is a little bit longer then others. It's just for testing...",
          price: 0.8,
          quantity: 8
        },
        {
          id: 4,
          groupID: 0,
          name: "dadasss",
          description: "test description. It is a little bit longer then others. It's just for testing...",
          price: 0.8,
          quantity: 8
        },
        {
          id: 5,
          groupID: 0,
          name: "dadasss",
          description: "test description. It is a little bit longer then others. It's just for testing...",
          price: 0.8,
          quantity: 8
        },
        {
          id: 6,
          groupID: 0,
          name: "dadasss",
          description: "test description. It is a little bit longer then others. It's just for testing...",
          price: 0.8,
          quantity: 8
        }
      ]

    }
  }


  handleOnclick(event)  {
    console.log(event)
  }

  toggle(e) {
    this.setState({
      [e]: !this.state[e]
    });
  }

  render() {
    return (
      <div>
        <PanelHeader size="sm" />
        <div className="content">
          <Row>
            <Col xs={12}  md={4}>
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">Good groups and goods</CardTitle>
                </CardHeader>
                <CardBody>

                      <Table responsive className="table-hover" id={"groups"}>
                        <thead className="text-primary">
                        <tr>
                          {goodGroupsTableHeader.map((prop, key) => {
                            if (key === goodGroupsTableHeader.length - 1) {
                              return <th key={key} className="text-sm-right text-success">{prop.name}</th>;
                            } else {
                              return <th key={key}>{prop.name}</th>;
                            }
                          })}
                        </tr>
                        </thead>
                        <tbody>
                        {
                          this.state.goodGroups.map((prop, key) => {
                          return (
                            <tr key={key} onClick={() => this.handleOnclick(prop)}>
                              <td key={key}>{prop.name}</td>
                              <td className="text-right">
                                <ButtonGroup className="btn-group-sm">
                                  <Button type="button" className="btn-primary" onClick={() => this.toggle("modalGroups")}>mod</Button>
                                  <Modal isOpen={this.state.modalGroups} toggle={() => this.toggle("modalGroups")} className={this.props.className}>
                                    <ModalHeader toggle={this.toggle}>Modal title</ModalHeader>
                                    <ModalBody>
                                      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                    </ModalBody>
                                    <ModalFooter>
                                      <Button color="primary" onClick={this.toggle}>Do Something</Button>{' '}
                                      <Button color="secondary" onClick={this.toggle}>Cancel</Button>
                                    </ModalFooter>
                                  </Modal>

                                  <Button color="danger" onClick={() => this.toggle("modalGoods")}>del</Button>
                                  <Modal isOpen={this.state.modalGoods} toggle={() => this.toggle("modalGoods")} className="modal-dialog-centered">
                                    <ModalHeader toggle={this.toggle} className="text-danger">
                                      Mal qrupu silinecek!<br/>Davam edilsin?
                                    </ModalHeader>
                                    <ModalFooter>
                                      <Button color="primary" onClick={this.toggle}>Sil</Button>{' '}
                                      <Button color="secondary" onClick={this.toggle}>Legv et</Button>
                                    </ModalFooter>
                                  </Modal>
                                </ButtonGroup>
                              </td>
                            </tr>
                          )})
                        }
                        </tbody>
                      </Table>
                </CardBody>
              </Card>

            </Col>
            <Col xs={12} md={8}>
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">Good groups and goods</CardTitle>
                </CardHeader>
                <CardBody>
                      <Table responsive className="table-hover" id={"goods"}>
                        <thead className="text-primary">
                        <tr>
                          {goodsTableHeader.map((prop, key) => {
                            if (key === goodsTableHeader.length - 1) {
                              return <th key={key} className="text-sm-right text-success">{prop.name}</th>;
                            } else {
                              return <th key={key}>{prop.name}</th>;
                            }
                          })}
                        </tr>
                        </thead>
                        <tbody>
                        {this.state.goods.map((prop, key) => {
                          return (
                            <tr key={key} onClick={() => this.handleOnclick(prop)}>
                              {goodsTableHeader.map((prope, key) => {
                                if (prope.name === "Actions") {
                                  return <td className="text-right">
                                    <ButtonGroup className="btn-group-sm">
                                      <Button type="button" className="btn-primary">ed</Button>
                                      <Button type="button" className="btn-primary">del</Button>
                                    </ButtonGroup>
                                  </td>
                                } else {
                                  return <td key={key}>{prop[prope.id]}</td>;
                                }
                              })}
                            </tr>
                          );
                        })}
                        </tbody>
                      </Table>
                </CardBody>
              </Card>

            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export default RegularTables;
