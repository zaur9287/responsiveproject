import React from "react";
import {
  Button,
  Modal,
  ModalFooter,
  ModalHeader
} from "reactstrap";
import axios from 'axios';

class DeleteModal extends React.Component {
  constructor(props) {
    super();
    this.handleDelete = this.handleDelete.bind(this);
    this.toggle = this.toggle.bind(this);
    this.state = {
      modal: false
    }
  }

  handleDelete(user) {
    axios({
      method: 'delete',
      url: axios.defaults.baseURL + `/user/` + user.userID
    }).then(res => {
      if (res.status === 200) {
        console.log("user deleted");
      }
    }).catch(res => {
      console.log("Catched error from me\n  please handle me!\n" + res);
    });
  }

  toggle(user) {
    this.setState({
      modal: !this.state.modal
    })
  }

  render() {
    return (
      <div>
        <Button className="btn-sm bg-danger" onClick={this.toggle}>del</Button>
        <Modal isOpen={this.state.modal} className="modal-dialog-centered">
          <ModalHeader className="text-danger">
            User will delete!<br/>
            Continue?
          </ModalHeader>
          <ModalFooter>
            <Button color="primary" onClick={() => this.handleDelete(this.props.user)}>Delete</Button>{' '}
            <Button color="secondary" onClick={this.toggle}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default DeleteModal;