import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  ModalHeader,
  ModalBody,
  ModalFooter,
  Modal,
  Button,
  Input,
  Form,
  FormGroup,
  Label
} from 'reactstrap';

function isEquivalent(a, b) {
  // Create arrays of property names
  var aProps = Object.getOwnPropertyNames(a);
  var bProps = Object.getOwnPropertyNames(b);

  // If number of properties is different,
  // objects are not equivalent
  if (aProps.length != bProps.length) {
    return false;
  }

  for (var i = 0; i < aProps.length; i++) {
    var propName = aProps[i];

    // If values of same property are not equal,
    // objects are not equivalent
    if (a[propName] !== b[propName]) {
      return false;
    }
  }

  // If we made it this far, objects
  // are considered equivalent
  return true;
}

class UserEdit extends Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this)
    this.handleEdit = this.handleEdit.bind(this)
    this.checkClose = this.checkClose.bind(this)
    this.onExit = this.onExit.bind(this)
  }

  state = {
    modal: false,
    isNew: false,
    user: {...this.props.user}
  };

  static propTypes = {
    user: PropTypes.shape({
      fullName: PropTypes.string.isRequired,
      email: PropTypes.string.isRequired
    }).isRequired
  };

  handleEdit(event) {
    this.setState({
      user: {
        ...this.state.user,
        [event.target.name]: event.target.value
      }
    })
  }

  toggle() {
    this.setState({
      modal:  !this.state.modal
    })
  };

  onExit() {
    this.setState({
      ...this.state,
      user: this.props.user
    });
    this.toggle();
  };

  componentWillUpdate(nextProps, nextState) {
    let ns = nextState;
    if ( !ns.modal && (ns.user.fullName === "" || ns.user.email === "") ){
      alert("You must fill the fullname and email blanks.")
    }
    else if ( !ns.modal && !isEquivalent(this.props.user, ns.user) ) {
      console.log("update user")
    }
  }

  checkClose() {
    let ns = this.state;
    if ( ns.modal && (ns.user.fullName === "" || ns.user.email === "") ){
      alert("You must fill the fullname and email blanks.")
      return false;
    }
    this.toggle();
    return true;
  }


  render() {
    return (
      <div>
        <Button className="btn-sm btn-outline-danger text-light" onClick={ this.toggle }>ed</Button>
        <Modal isOpen={this.state.modal} className="modal-dialog-centered">
          <ModalHeader className="text-danger">
            Edit user
          </ModalHeader>
          <ModalBody>
            <div className="content">
                <Form>
                  <FormGroup>
                    <Label for="fullName">Full name</Label>
                    <Input
                      type="email"
                      name="fullName"
                      id="fullName"
                      placeholder="Insert your full name"
                      value={this.state.user.fullName}
                      onChange={this.handleEdit}
                      required={true}
                    />
                  </FormGroup>

                  <FormGroup>
                    <Label for="email">Email</Label>
                    <Input
                      type="email"
                      name="email"
                      id="email"
                      placeholder="Insert your email."
                      value={this.state.user.email}
                      onChange={this.handleEdit}
                      required={true}
                    />
                  </FormGroup>

                  <FormGroup>
                    <Label for="address">Address</Label>
                    <Input
                      type="text"
                      name="address"
                      id="address"
                      placeholder="Insert your address."
                      value={this.state.user.address}
                      onChange={this.handleEdit}
                    />
                  </FormGroup>

                  <FormGroup>
                    <Label for="description">Description</Label>
                    <Input
                      type="textarea"
                      name="description"
                      id="description"
                      placeholder="Your description is here."
                      value={this.state.user.description}
                      onChange={this.handleEdit}
                    />
                  </FormGroup>
              </Form>
            </div>
          </ModalBody>
          <ModalFooter>
            <Button color="info" onClick={ this.checkClose } >Save</Button>{' '}
            <Button color="secondary" onClick={ this.onExit }>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default UserEdit;
