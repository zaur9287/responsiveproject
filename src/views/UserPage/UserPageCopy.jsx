import React from "react";
import { Card, CardHeader, CardBody, Row, Col } from "reactstrap";
import axios from 'axios';

import { PanelHeader, FormInputs, CardAuthor } from "components";

import userBackground from "assets/img/bg5.jpg";
import userAvatar from "assets/img/mike.jpg";


class User extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {
        "userID": "",
        "loginInfo": {
          "providerID": "",
          "providerKey": ""
        },
        "fullName": "",
        "email": "",
        "activated": false,
        "createdAt": "",
        updatedAt: "",
        companyID: 0,
        company: {
          "id": 0,
          "name": "",
          "imageID": 0,
          "createdAt": "",
          "updatedAt": ""
        },
        owner: false,
        address: ""
      }
    }
  }


  componentDidMount() {
    axios({
      method: 'get',
      url: axios.defaults.baseURL + `/me`
    }).then(res => {
      if (res.status === 200) {
        this.setState({
          user: res.data
        })
      }
    }).catch(res => {
      console.log("Catched error from me\n" + res);
    });
  }


  render() {
    return (
      <div>
        <PanelHeader size="sm" />
        <div className="content">
          <Row>
            <Col md={8} xs={12}>
              <Card>
                <CardHeader>
                  <h5 className="title">Edit Profile</h5>
                </CardHeader>
                <CardBody>
                  <form>
                    <FormInputs
                      ncols={[
                        "col-md-6 pr-1",
                        "col-md-6 pl-1"
                      ]}
                      proprieties={[
                        {
                          label: "Company (disabled)",
                          inputProps: {
                            type: "text",
                            disabled: true,
                            placeholder: "Company name",
                            defaultValue: this.state.user.company.name
                          }
                        },
                        {
                          label: "Email address",
                          inputProps: {
                            type: "email",
                            placeholder: "Email",
                            defaultValue: this.state.user.email
                          }
                        }
                      ]}
                    />
                    <FormInputs
                      ncols={["col-md-12"]}
                      proprieties={[
                        {
                          label: "Full Name",
                          inputProps: {
                            type: "text",
                            placeholder: "Full Name",
                            defaultValue: this.state.user.fullName
                          }
                        }
                      ]}
                    />
                    <FormInputs
                      ncols={["col-md-12"]}
                      proprieties={[
                        {
                          label: "Address",
                          inputProps: {
                            type: "text",
                            placeholder: "Home Address",
                            defaultValue: this.state.user.address
                          }
                        }
                      ]}
                    />
                    <FormInputs
                      ncols={["col-md-12"]}
                      proprieties={[
                        {
                          label: "About Me",
                          inputProps: {
                            type: "textarea",
                            rows: "4",
                            cols: "80",
                            defaultValue:
                              "I'm glad to be here for serving you. Thanks for using this app.",
                            placeholder: "Here can be your description"
                          }
                        }
                      ]}
                    />
                  </form>
                </CardBody>
              </Card>
            </Col>
            <Col md={4} xs={12}>
              <Card className="card-user">
                <div className="image">
                  <img src={userBackground} alt="..." />
                </div>
                <CardBody>
                  <CardAuthor
                    avatar={userAvatar}
                    avatarAlt="..."
                    title={this.state.user.fullName}
                  />
                  <p className="description text-center">
                    "description text bura gelecek <br />
                    Your chick she so thirsty <br />
                    I'm in that two seat Lambo"
                  </p>
                </CardBody>
                {/*<hr />*/}
                {/*<CardSocials*/}
                  {/*size="lg"*/}
                  {/*socials={[*/}
                    {/*{*/}
                      {/*icon: "fab fa-facebook-f",*/}
                      {/*href: "https://www.facebook.com/"*/}
                    {/*},*/}
                    {/*{*/}
                      {/*icon: "fab fa-twitter",*/}
                      {/*href: "https://www.facebook.com/"*/}
                    {/*},*/}
                    {/*{*/}
                      {/*icon: "fab fa-google-plus-g",*/}
                      {/*href: "https://plus.google.com/discover"*/}
                    {/*}*/}
                  {/*]}*/}
                {/*/>*/}
              </Card>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export default User;
