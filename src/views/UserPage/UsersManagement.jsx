import React from "react";
import {
  Card,
  CardHeader,
  CardBody,
  Row,
  Col,
  Table,
  ButtonGroup
} from "reactstrap";
import axios from 'axios';
import DeleteModal from './DeleteModal';
import UserEdit from './UserEdit';

import { PanelHeader } from "components";


const tableHeaders = [
  {
    id: "fullName",
    name: "Full name"
  },
  {
    id: "email",
    name: "Email"
  },
  {
    id: "address",
    name: "Address"
  },
  {
    id: "description",
    name: "Description"
  },
  {
    id: "actions",
    name: "Actions"
  }
];

class UsersManagement extends React.Component {
  constructor(props) {
    super(props);
    this.handleDelete = this.handleDelete.bind(this);
    this.state = {
      deleteModal: false,
      isLoading: false,
      userList: [
        {
          "userID": "12",
          "loginInfo": {
            "providerID": "email",
            "providerKey": "zaur.nerimanli@gmail.com"
          },
          "fullName": "Zaur Nerimanli",
          "email": "zaur.nerimanli@gmail.com",
          "activated": true,
          "createdAt": "121212",
          updatedAt: "12121212",
          companyID: 1,
          owner: false,
          address: "harasa biryer",
          description: ""
        },
        {
          "userID": "13",
          "loginInfo": {
            "providerID": "email",
            "providerKey": "nruaz@mail.ru"
          },
          "fullName": "Nruaz",
          "email": "nruaz@mail.ru",
          "activated": true,
          "createdAt": "1212121",
          updatedAt: "121212121",
          companyID: 1,
          owner: false,
          address: "nruaz text address",
          description: "my desc nruaz"
        },
        {
          "userID": "14",
          "loginInfo": {
            "providerID": "email",
            "providerKey": "zaur9287@gmail.com"
          },
          "fullName": "Zaur 9287",
          "email": "zaur9287@gmail.com",
          "activated": true,
          "createdAt": "121212",
          updatedAt: "12121212",
          companyID: 1,
          owner: false,
          address: "harasa biryer 9287",
          description: ""
        }
      ]
    }
  }


  componentDidMount() {
    axios({
      method: 'get',
      url: axios.defaults.baseURL + `/list`
    }).then(res => {
      if (res.status === 200) {
        this.setState({
          userList: res.data
        })
      }
    }).catch(res => {
      console.log("Catched error from me\n" + res);
    });
  }

  handleDelete(event) {
    axios({
      method: 'delete',
      url: axios.defaults.baseURL + `/user/` + event.target.name
    }).then(res => {
      if (res.status === 200) {
        console.log("user deleted");
      }
    }).catch(res => {
      console.log("Catched error from me\n" + res);
    });
  }

  render() {
    return (
      <div>
        <PanelHeader size="sm" />
        <div className="content">
          <Row>
            <Col md={12} xs={12}>
              <Card>
                <CardHeader>
                  <h5 className="title">Users Management
                    <button className="btn-outline-dark btn-light float-md-right">Add user</button>
                  </h5>
                </CardHeader>
                <CardBody>

                  <Table responsive className="table-hover" id={"groups"}>
                    <thead className="text-primary">
                    <tr>
                      {tableHeaders.map((prop, key) => {
                        if (key === tableHeaders.length - 1) {
                          return <th key={key} className="text-sm-right text-success">{prop.name}</th>;
                        } else {
                          return <th key={key}>{prop.name}</th>;
                        }
                      })}
                    </tr>
                    </thead>
                    <tbody>
                    {this.state.userList.map((user, key) => {
                      return (
                        <tr key={key}>
                          {tableHeaders.map((prope, key) => {
                            if (prope.name === "Actions") {
                              return <td key={key} className="text-right">
                                <ButtonGroup className="btn-group-sm">
                                  {/*<Button type="button" className="btn-primary">ed</Button>*/}
                                  <UserEdit user={user}/>
                                  <DeleteModal user={user} new={false}/>
                                </ButtonGroup>
                              </td>
                            } else {
                              return <td key={key}>{user[prope.id]}</td>;
                            }
                          })}
                        </tr>
                      );
                    })}
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export default UsersManagement;
